# uni-app零基础到uniCloud实战

## 介绍

[uni-app官网](https://uniapp.dcloud.io/)

uni-app是一个基于Vue.js开发所有前端应用的框架，包括iOS、Android以及各大平台小程序。

学会uni-app意味着你将能天下无敌(那是不可能的)。

加上uniCloud的后端能力以及uni-app自身强大的插件市场，快速的开发一个应用不再是苦逼的。


## 本课程学习路线

uni-app零基础到uniCloud，系统学习全家桶，同时为大家准备了视频教程。

[b站视频链接](https://www.bilibili.com/video/BV1u34y1z71d/)


[上一个版本课程](https://www.bilibili.com/video/BV1yV411j7S6/)


![plan](https://gitee.com/dmhsq/photos/raw/master/one.png)

为了能让大家更好的学习uni-app，更好的练习，本次我们有代码的课程将会为大家提供代码。

代码仓库地址为 https://gitee.com/dmhsq/uni-app-edu-code.git

执行

```
git clone https://gitee.com/dmhsq/uni-app-edu-code.git
```

当然你可能什么也没有clone到，因为我还没有push。焯。


## 学习技巧

1.学会看文档，养成不会了先查文档，查不到再问。

2.学会抄代码，比着文档示例多写几遍。

3.学会练习，熟能生巧，眼：我学会了，手：不，你没有。

4.学会调试，发现问题要多看看控制台，网络请求往往会告诉你问题。

## uni-app基础

#### 建立开发环境

[下载Hbuilder X](https://www.dcloud.io/hbuilderx.html)

##### 微信开发者工具

下载地址： [微信开发者工具](https://developers.weixin.qq.com/miniprogram/dev/devtools/download.html)

安装完成后 在Hbuilder 工具->设置->运行配置微信开发者工具路径 为刚才安装时路径

##### 模拟器/手机

模拟器自行选择

本课程使用的是逍遥安卓模拟器

[下载逍遥安卓模拟器](https://www.xyaz.cn/)

如果是逍遥安卓模拟器 在Hbuilder X 工具->设置->运行配置 安卓模拟器端口输入21503

其他模拟器百度搜索

如果你有可用于测试的手机，可以下载adb，然后运行到手机即可。

[adb下载地址](https://developer.android.com/studio/releases/platform-tools.html)

解压后复制解压路径 xxxxxxx/platform-tools 添加到系统环境变量


#### 创建一个项目并且运行

文件-> 新建 -> 项目 ->uniapp选择创建

运行的话根据需求运行到不同平台


#### 组件简介以及组件库推荐

练习使用可以创建模板来观察官方如何使用

uni-ui
建议模板直接引入

单个使用的话插件市场引入



引入uView-ui

1.插件市场引入

App.vue
```
<style lang="scss">
	/*每个页面公共css */
	@import "@/uni_modules/uview-ui/index.scss";
	
</style>
```

main.js

```
import uView from '@/uni_modules/uview-ui'
Vue.use(uView)
```

uni.scss

```
@import '@/uni_modules/uview-ui/theme.scss';
```

pages.json

```
"easycom": {
		// npm安装的方式不需要前面的"@/"，下载安装的方式需要"@/"
		// npm安装方式
		"^u-(.*)": "@/uni_modules/uview-ui/components/u-$1/u-$1.vue"
		// 下载安装方式
		// "^u-(.*)": "@/uview-ui/components/u-$1/u-$1.vue"
	},
	"pages": [ //pages数组中第一项表示应用启动页，参考：https://uniapp.dcloud.io/collocation/pages
		{
			"path": "pages/index/index",
			....................
```

2.官网引入
类似上面的插件市场引入 不过注意路径

#### Api简介以及使用

[文档](https://uniapp.dcloud.io/api/request/request)

uni.request({
	
})


##### 跨域

配置vue.config.js
```
module.exports = {
  devServer: {
    proxy: {
      "/api": {
        target: "https://xxxxx",
        changeOrigin: true,
        pathRewrite: {
          "^/api": ""
        }
      }
    }
  }
};

```

##### 跳转页面
注意区别

练习配置 再pages.json下配置一个tarBar

```js
"tabBar": {
	"list": [{
	    "pagePath": "pages/index/index",
	    "text": "index"
	}, {
	    "pagePath": "pages/one/one",
	    "text": "one"
	}]
}
```

###### uni.navigateTo(OBJECT)

保留当前页面，跳转到应用内的某个页面

###### uni.redirectTo(OBJECT)

关闭当前页面，跳转到应用内的某个页面

###### uni.reLaunch(OBJECT)

关闭所有页面，打开到应用内的某个页面。

###### uni.switchTab(OBJECT)

跳转到 tabBar 页面，并关闭其他所有非 tabBar 页面

###### uni.navigateBack(OBJECT)

关闭当前页面，返回上一页面或多级页面

######  路由传参

uni.navigateTo({
    url: 'xxxxx?xxx=xxx&xxx=xxx'
});


###### 页面通讯

请给我数据 two

收到，准备返回数据 two

请接受 index

已接受 index


```js
index.vue
uni.navigateTo({
	url: '../two/two',
	events: {
		recive: function(data) {
			console.log(data.data)
			console.log("已接受")
		}
	},
	success: function(res) {
		res.eventChannel.emit('send', {
			data: '请给我数据'
		})
	}
})

```


```js
two.vue
onLoad() {
	const eventChannel = this.getOpenerEventChannel();
	eventChannel.on('send', function(data) {
		console.log(data.data)
		console.log("收到，准备返回数据")
	})
	eventChannel.emit('recive', {
		data: "请接受"
	});
}

```

##### 数据缓存

[官网地址](https://uniapp.dcloud.io/api/storage/storage?id=setstorage)

``` js
<template>
	<view>
		<button @click="setStorage()">点我加storage</button>
		<button @click="getStorage()">点我获取storage</button>
		<button @click="removeStorage()">点我删除storage:age</button>
		<button @click="clearStorage()">点我清空storage</button>
	</view>
</template>

<script>
	export default {
		data() {
			return {
				
			}
		},
		methods: {
			clearStorage(){
				// uni.clearStorage()
				try {
					uni.clearStorageSync()
				} catch(e){
					console.log(e)
				}
			},
			removeStorage(){
				// uni.removeStorage({
				// 	key: 'age',
				// 	success:function(res){
				// 		console.log(res)
				// 	}
				// })
				// try {
				// 	let res = uni.removeStorageSync('age');
				// 	console.log(res)
				// } catch(e){
				// 	console.log(e)
				// }
			},
			setStorage(){
				// uni.setStorage({
				// 	key: 'name',
				// 	data: 'dmhsq1',
				// 	success:function(res){
				// 		console.log(res)
				// 	}
				// })
				uni.setStorageSync('age',300)
			},
			getStorage(){
				// uni.getStorage({
				// 	key: 'name',
				// 	success:function(res){
				// 		console.log(res)
				// 	}
				// })
				// let age = uni.getStorageSync('age');
				// console.log(age)
				// uni.getStorageInfo({
				// 	success:function(res){
				// 		console.log(res)
				// 	}
				// })
				// try {
				// 	let info =uni.getStorageInfoSync();
				// 	console.log(info)
				// } catch (e) {
				// 	console.log(e)
				// }
			}
		}
	}
</script>

<style>

</style>

```

#### 小程序发布注意事项
#### H5部署到云


## 


## 



## 



## 常见问题

1.Q:你这uni-app保熟吗？
nonono，这里不卖瓜，不保熟。

2.为什么我的云函数啥的没有返回
because: 注意云函数的return。

3.注意切换本地或者云端云函数。

